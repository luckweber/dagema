<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css" />

  <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
  <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>


<nav class="navbar navbar-inverse " role="navigation">
<div class="navbar-inner"> <!--changes made here-->
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">

                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
            <a class="navbar-brand" href="#">
                <img src="<?php bloginfo('template_url'); ?>/img/logo.png" alt="">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#">HELLO</a></li>
				<li><a href="#">ABOUT</a></li>
				<li><a href="#">SERVICES</a></li>
				<li><a href="#">PORTIFOLIO</a></li>
				<li><a href="#">TEAM</a></li>
				<li><a href="#">CONTACT</a></li>
            </ul>
        </div>
    </div>
</div>
</nav>


<div class="carousel fade-carousel slide carousel-fade" data-ride="carousel" data-interval="4000" id="bs-carousel">
  <!-- Overlay -->

  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
    <li data-target="#bs-carousel" data-slide-to="1"></li>
    <li data-target="#bs-carousel" data-slide-to="2"></li>
  </ol>
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item slides active">
      <div class="slide-1">
          <div class="overlay"></div>
      </div>
      <div class="hero">
        <hgroup>
            <h1>CREATIVE SOLUTIONS</h1>        
            <h3>GOOD THAT LESSER. STARS HEAVEN OVER SET MAY HERING, IS ALL LAND CAN'T YOU'RE AFTER LIGHT OUR DARKNESS, THING ISN'T</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg" role="button">Découvrir</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-2">
          <div class="overlay"></div>
      </div>
      <div class="hero">        
        <hgroup>
            <h1>CREATIVE SOLUTIONS</h1>        
            <h3>GOOD THAT LESSER. STARS HEAVEN OVER SET MAY HERING, IS ALL LAND CAN'T YOU'RE AFTER LIGHT OUR DARKNESS, THING ISN'T</h3>
        </hgroup>       
        <button class="btn btn-hero btn-lg" role="button">Découvrir</button>
      </div>
    </div>
    <div class="item slides">
      <div class="slide-3">
          <div class="overlay"></div>
      </div>
      <div class="hero">        
        <hgroup>
            <h1>CREATIVE SOLUTIONS</h1>        
            <h3>GOOD THAT LESSER. STARS HEAVEN OVER SET MAY HERING, IS ALL LAND CAN'T YOU'RE AFTER LIGHT OUR DARKNESS, THING ISN'T</h3>
        </hgroup>
        <button class="btn btn-hero btn-lg" role="button">See all features</button>
      </div>
    </div>
  </div> 
  
    <div class="main-border"></div>

</div>