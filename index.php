<?php
/* Template name: Template DaGema */



 get_header(); 
 
 
 
 
 ?>

<div class=" container main">
  <h2><img src="<?php bloginfo('template_url'); ?>/img/about.png" /></h2>
  <h3>Nous sommes une agence degital complete de service</h3>
  <h3>Nous concevons et realisons des SITES, APPLICATIONS SOLUTIONS et autres</h3>
  <h4>Click on the images to enlarge them.</h4>
  <h4>Click on the images to enlarge them.</h4>
  <div class="row">
    <div class="col-md-3">
      <div class="thumbnail">
        <a href="/w3images/lights.jpg" target="_blank">
          <img src="<?php bloginfo('template_url'); ?>/img/lampada.png" alt="Lights" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
        <a href="<?php bloginfo('template_url'); ?>/img/lampada.png" target="_blank">
          <img src="<?php bloginfo('template_url'); ?>/img/oculos.png" alt="Nature" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-3">
      <div class="thumbnail">
        <a href="/w3images/fjords.jpg" target="_blank">
          <img src="<?php bloginfo('template_url'); ?>/img/config.png" alt="Fjords" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
	 <div class="col-md-3">
      <div class="thumbnail">
        <a href="/w3images/fjords.jpg" target="_blank">
          <img src="<?php bloginfo('template_url'); ?>/img/power.png" alt="Fjords" style="width:100%">
          <div class="caption">
            <p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>


<div class="container service">    
  <h3><img src="<?php bloginfo('template_url'); ?>/img/service.png" /></h3><br>
  <div class="row">
    <div class="col-md-2">
        <p align='left' data-toggle="tooltip" data-html="true" title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><a title="Image 1" href="#"><img src="<?php bloginfo('template_url'); ?>/img/service-mobile.png" class=" img-responsive img1-hover" style="width:100%" alt="Image"></a></p>
		<p class="service-title">MOBILE</p>
    </div>
    <div class="col-md-2"> 
      <p align='left' data-toggle="tooltip" data-html="true" title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/service-design.png" class="img-responsive img2-hover" style="width:100%" alt="Image"></p>
		<p class="service-title">DESIGN</p>    
    </div>
	<div class="col-md-2">
        <p align='left' data-toggle="tooltip" data-html="true" title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><a title="Image 1" href="#"><img src="<?php bloginfo('template_url'); ?>/img/service-mobile.png" class=" img-responsive img1-hover" style="width:100%" alt="Image"></a></p>
		<p class="service-title">MOBILE</p>
    </div>
    <div class="col-md-2"> 
      <p align='left' data-toggle="tooltip" data-html="true" title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/service-design.png" class="img-responsive img2-hover" style="width:100%" alt="Image"></p>
		<p class="service-title">DESIGN</p>    
    </div>
	<div class="col-md-2">
        <p align='left' data-toggle="tooltip" data-html="true" title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><a title="Image 1" href="#"><img src="<?php bloginfo('template_url'); ?>/img/service-mobile.png" class=" img-responsive img1-hover" style="width:100%" alt="Image"></a></p>
		<p class="service-title">MOBILE</p>
    </div>
  </div>
</div>

<div class="container"> 
	 <div class="row">
		<div class="col-md-2">
			<div class="line-template"></div>
		</div>
	 </div>
</div>	 
		

<div class="container portfolio">    
  <h3><img src="<?php bloginfo('template_url'); ?>/img/a.png" /></h3><br>
  <div class="row">
    <div class="col-md-4">
        <p align='left'  title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/foto1.png" class="img-responsive" style="width:100%" alt="Image"></p>
		<p class="portfolio-title">Flat Pixel</p>   
		<p class="portfolio-subtitle">Website</p>
		<div class="portfolio-hidden">
			<p class="portfolio-title">Flat Pixel</p>   
			<p class="portfolio-subtitle">Website</p>
		</div>
    </div>
    <div class="col-md-4"> 
      <p align='left'  title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/foto2.png" class="img-responsive" style="width:100%" alt="Image"></p>
		<p class="portfolio-title">Flat Pixel</p>   
		<p class="portfolio-subtitle">Website</p> 
		<div class="portfolio-hidden">
			<p class="portfolio-title">Flat Pixel</p>   
			<p class="portfolio-subtitle">Website</p>
			<div class="portfolio-hidden">
				<p class="portfolio-title">Flat Pixel</p>   
				<p class="portfolio-subtitle">Website</p>
			</div>

		</div>
		
    </div>
	<div class="col-md-4"> 
		<p align='left' title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/foto3.png" class="img-responsive" style="width:100%" alt="Image"></p>
		<p class="portfolio-title">Flat Pixel</p>   
		<p class="portfolio-subtitle">Website</p> 
		<div class="portfolio-hidden">
			<p class="portfolio-title">Flat Pixel</p>   
			<p class="portfolio-subtitle">Website</p>
		</div>
		
    </div>
	<div class="col-md-4"> 
      <p align='left'title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/foto4.png" class="img-responsive" style="width:100%" alt="Image"></p>
		<p class="portfolio-title">Flat Pixel</p>   
		<p class="portfolio-subtitle">Website</p> 
		<div class="portfolio-hidden">
			<p class="portfolio-title">Flat Pixel</p>   
			<p class="portfolio-subtitle">Website</p>
		</div>
		
    </div>
	<div class="col-md-4"> 
      <p align='left'  title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/foto5.png" class="img-responsive" style="width:100%" alt="Image"></p>
		<p class="portfolio-title"> Flat Pixel</p>   
		<p class="portfolio-subtitle">Website</p> 
		<div class="portfolio-hidden">
			<p class="portfolio-title">Flat Pixel</p>   
			<p class="portfolio-subtitle">Website</p>
		</div>
		
    </div>
	<div class="col-md-4"> 
      <p align='left' title = "Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec<br>Lorem ipsum donec"><img src="<?php bloginfo('template_url'); ?>/img/foto1.png" class="img-responsive" style="width:100%" alt="Image"></p>
		<p class="portfolio-title">Flat Pixel</p>   
		<p class="portfolio-subtitle">Website</p> 
		<div class="portfolio-hidden">
			
			<p class="portfolio-title">Flat Pixel</p>   
			<p class="portfolio-subtitle">Website</p>
		</div>
		
    </div>
	
  </div>
</div><br>

<?php get_footer(); ?>