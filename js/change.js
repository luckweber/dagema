 jQuery( document ).ready( function($){
	
	$('[data-toggle="tooltip"]').tooltip();
	
	
	$("body").on("mouseover",".portfolio > .row  > .col-md-4", function(){
		$(this).each(function(){
			$(this).find(".portfolio-hidden").css("visibility", "visible");
			
			
		});
	});
	
	
	$("body").on("mouseout",".portfolio > .row  > .col-md-4", function(){
		$(this).each(function(){
			$(this).find(".portfolio-hidden").css("visibility", "hidden");
		});
	});
	
	
	$("body").on("mouseover",".img1-hover", function(){
	
		$(this).each(function(){
			$(this).attr("src","img/service-mobile-hover.png");
		
		});
		
	});
	
	$("body").on("mouseout",".img1-hover", function(){
		$(this).each(function(){
			$(this).attr("src","img/service-mobile.png");
		
		});
		
	});
	
	
	$("body").on("mouseover",".img2-hover", function(){
	
		$(this).each(function(){
			$(this).attr("src","img/service-design-hover.png");
		
		});
		
	});
	
	$("body").on("mouseout",".img2-hover", function(){
		$(this).each(function(){
			$(this).attr("src","img/service-design.png");
		
		});
		
	});
 
 
 });