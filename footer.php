<footer class="container-fluid text-center">
  <p class="footer-image"><img src="<?php bloginfo('template_url'); ?>/img/footer.png" /></p>
</footer>

</body>

<script>
jQuery(document).ready(function($){
    $('[data-toggle="tooltip"]').tooltip();
	var templateUrl = '<?= get_bloginfo("template_url"); ?>';
	
	
	$("body").on("mouseover",".portfolio > .row  > .col-md-4", function(){
		$(this).each(function(){
			$(this).find(".portfolio-hidden").css("visibility", "visible");
			
			
		});
	});
	
	
	$("body").on("mouseout",".portfolio > .row  > .col-md-4", function(){
		$(this).each(function(){
			$(this).find(".portfolio-hidden").css("visibility", "hidden");
		});
	});
	
	
	$("body").on("mouseover",".img1-hover", function(){
	
		$(this).each(function(){
			$(this).attr("src",templateUrl+"/img/service-mobile-hover.png");
		
		});
		
	});
	
	$("body").on("mouseout",".img1-hover", function(){
		$(this).each(function(){
			$(this).attr("src",templateUrl+"/img/service-mobile.png");
		
		});
		
	});
	
	
	$("body").on("mouseover",".img2-hover", function(){
	
		$(this).each(function(){
			$(this).attr("src",templateUrl+"/img/service-design-hover.png");
		
		});
		
	});
	
	$("body").on("mouseout",".img2-hover", function(){
		$(this).each(function(){
			$(this).attr("src",templateUrl+"/img/service-design.png");
		
		});
		
	});
	
});
</script>
</html>
